package com.jila.auslink

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jila.auslink.databinding.ActivityMyStoryBinding

class MyStoryActivity : AppCompatActivity() {
    private val mAdapter = StoryAdapter()
    private lateinit var binding: ActivityMyStoryBinding
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEditMystory.setOnClickListener {
            val intent = Intent(this, EditMyStoryActivity::class.java)
            startActivity(intent)
        }

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            val userid = FirebaseAuth.getInstance().currentUser?.uid.toString()
            dbRef.child("story").child(userid).addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val liststory = arrayListOf<Story>()

                        snapshot.children.forEach {
                            it.getValue(StoryResponse::class.java)?.let { response ->
                                val transformedResponse = mapResponseToStory(response)
                                liststory.add(transformedResponse)
                            }
                        }

                        mAdapter.setData(liststory)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@MyStoryActivity, error.message, Toast.LENGTH_SHORT)
                            .show()
                    }

                }
            )
        } catch (e: Exception) {
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvItemMystory) {
            layoutManager = LinearLayoutManager(this@MyStoryActivity)
            adapter = mAdapter
        }
    }

    private fun mapResponseToStory(response: StoryResponse): Story =
        Story(
            addjudul = response.addjudul ?: "",
            addauthor = response.addauthor ?: "",
            addstory = response.addstory ?: ""
        )

    companion object {
        const val TAG_STORY = "TAG_STORY"
    }
}

