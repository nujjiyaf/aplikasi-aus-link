package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.jila.auslink.databinding.ActivityListStoryBinding

class ListStoryActivity : AppCompatActivity() {
    private val mAdapter = StoryAdapter()
    private lateinit var binding: ActivityListStoryBinding
    private lateinit var dbRef: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            dbRef.child("story").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listStory = arrayListOf<Story>()

                        snapshot.children.forEach { stories ->
                            stories.children.forEach {
                                it.getValue(StoryResponse::class.java)?.let { response ->
                                    val transformedResponse = mapResponseToStory(response)
                                    listStory.add(transformedResponse)
                                }
                            }
                        }
                        mAdapter.setData(listStory)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@ListStoryActivity, error.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            )
        } catch (e: Exception) {
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvItemStory) {
            layoutManager = LinearLayoutManager(this@ListStoryActivity)
            adapter = mAdapter
        }
    }

    private fun mapResponseToStory(response: StoryResponse): Story =
        Story(
            addjudul = response.addjudul ?: "",
            addauthor = response.addauthor ?: "",
            addstory = response.addstory ?: ""
        )
}