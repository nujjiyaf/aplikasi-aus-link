package com.jila.auslink

import com.google.gson.annotations.SerializedName

data class StoryResponse(
    @field:SerializedName("addstory")
    val addjudul: String? = null,
    @field:SerializedName("addauthor")
    val addauthor: String? = null,
    @field:SerializedName("addstory")
    val addstory: String? = null
)