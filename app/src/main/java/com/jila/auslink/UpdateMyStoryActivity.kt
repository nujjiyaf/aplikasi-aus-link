package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.jila.auslink.MyStoryActivity.Companion.TAG_STORY
import com.jila.auslink.databinding.ActivityUpdateMyStoryBinding

class UpdateMyStoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateMyStoryBinding
    private lateinit var story: Story
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateMyStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Story>(TAG_STORY)?.let {
            story = it
            updateUI(it)
        }

        binding.btnUpdateStory.setOnClickListener {
            val newStory = binding.updateStory.text.toString()
            val newJudul = binding.updateJudul.text.toString()
            story = story.copy(addjudul = newJudul, addstory = newStory)
            database = FirebaseDatabase.getInstance().getReference("story")
            FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                database.child(userid).child(story.addauthor).setValue(story)
                    .addOnSuccessListener {
                        binding.updateJudul.text.clear()
                        binding.updateStory.text.clear()

                        Toast.makeText(this, "Succesfully Update", Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }.addOnFailureListener {
                        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    private fun updateUI(story: Story) {
        binding.updateJudul.setText(story.addjudul)
        binding.updateAuthor.setText(story.addauthor)
        binding.updateStory.setText(story.addstory)
    }
}