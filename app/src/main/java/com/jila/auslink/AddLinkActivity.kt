package com.jila.auslink

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.jila.auslink.databinding.ActivityAddLinkBinding


class AddLinkActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var listgenre: Spinner
    private lateinit var liststatus: Spinner
    lateinit var binding: ActivityAddLinkBinding
    private lateinit var bottom_navigation: BottomNavigationView
    private lateinit var genre_spinner: String
    private lateinit var status_spinner: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddLinkBinding.inflate(layoutInflater)
        setContentView(binding.root)

        listgenre = findViewById(R.id.genre_spinner)
        liststatus = findViewById(R.id.status_spinner)

        bottom_navigation = binding.bottomNavigationView
        bottom_navigation.getMenu().findItem(R.id.addLinkActivity).setChecked(true);
        bottom_navigation.setOnNavigationItemSelectedListener(menuItemSelected)


        listgenre.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                genre_spinner = listgenre.getSelectedItem().toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        liststatus.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                status_spinner = liststatus.getSelectedItem().toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        binding.btnSave.setOnClickListener {
            if (isValidForm()) {
                val input_link = binding.inputLink.text.toString()
                val input_title = binding.inputTitle.text.toString()
                genre_spinner = genre_spinner
                status_spinner = status_spinner
                database = FirebaseDatabase.getInstance().getReference("link")
                FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                    val key = database.child(userid).child(genre_spinner).push().key.toString()
                    val link = Link(key, input_link, input_title, genre_spinner, status_spinner)
                    database.child(userid).child(genre_spinner).child(key).setValue(link)
                        .addOnSuccessListener {
                            binding.inputLink.text.clear()
                            binding.inputTitle.text.clear()

                            Toast.makeText(this, "Succesfully Added", Toast.LENGTH_SHORT).show()
                            onBackPressed()
                        }.addOnFailureListener {
                            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                        }
                }
            } else {
                Toast.makeText(this, "Data not filled", Toast.LENGTH_SHORT).show()
            }
        }


    }

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.addLinkActivity -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.genreActivity -> {
                val intent = Intent(this, GenreActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.homepageActivity -> {
                val intent = Intent(this, HomepageActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.logoutActivity -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun isValidForm(): Boolean =
        binding.inputTitle.text.isNotEmpty() && binding.inputLink.text.isNotEmpty()
}