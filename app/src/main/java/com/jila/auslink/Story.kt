package com.jila.auslink

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Story(val addjudul: String, val addauthor: String, val addstory: String) : Parcelable
