package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jila.auslink.databinding.ActivityEditMyStoryBinding

class EditMyStoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEditMyStoryBinding
    private val mAdapter = MyStoryAdapter()
    private lateinit var dbRef: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditMyStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            val userid = FirebaseAuth.getInstance().currentUser?.uid.toString()
            dbRef.child("story").child(userid).addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val liststory = arrayListOf<Story>()
                        snapshot.children.forEach {
                            it.getValue(MyStoryResponse::class.java)?.let { response ->
                                val transformedResponse = mapResponseToStory(response)
                                liststory.add(transformedResponse)
                            }
                        }

                        mAdapter.setData(liststory)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(
                            this@EditMyStoryActivity,
                            error.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            )
        } catch (e: Exception) {
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvItemDeletestory) {
            layoutManager = LinearLayoutManager(this@EditMyStoryActivity)
            adapter = mAdapter
        }
    }

    private fun mapResponseToStory(response: MyStoryResponse): Story =
        Story(
            addjudul = response.addjudul ?: "",
            addauthor = response.addauthor ?: "",
            addstory = response.addstory ?: ""
        )

    companion object {
        const val TAG_STORY = "TAG_STORY"
    }
}