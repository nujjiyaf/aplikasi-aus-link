package com.jila.auslink

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jila.auslink.MyStoryActivity.Companion.TAG_STORY
import com.jila.auslink.databinding.ItemStoryBinding

class StoryAdapter : RecyclerView.Adapter<StoryAdapter.StoryViewHolder>() {
    private val data = arrayListOf<Story>()

    fun setData(newData: List<Story>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class StoryViewHolder(private val view: ItemStoryBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Story) {
            view.textJudulStory.text = data.addjudul
            view.textAuthor.text = data.addauthor
            view.btnReadStory.setOnClickListener {
                val intent = Intent(itemView.context, ViewStoryActivity::class.java).putExtra(
                    TAG_STORY,
                    data
                )
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryViewHolder =
        StoryViewHolder(
            ItemStoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}