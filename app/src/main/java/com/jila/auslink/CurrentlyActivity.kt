package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jila.auslink.databinding.ActivityCurrReadingBinding

class CurrentlyActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCurrReadingBinding
    private lateinit var dbRef: DatabaseReference
    private val mAdapter = LinkAdapter(this.javaClass.name)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCurrReadingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            val userid = FirebaseAuth.getInstance().currentUser?.uid.toString()
            dbRef.child("link").child(userid).addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listLink = arrayListOf<Link>()
                        snapshot.children.forEach { link ->
                            link.children.forEach {
                                it.getValue(LinkResponse::class.java)?.let { response ->
                                    val transformedResponse = mapResponseToLink(response)
                                    listLink.add(transformedResponse)
                                }
                            }

                        }
                        mAdapter.setData(listLink.filter { it.inputstatus == "Currently Reading" })
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(
                            this@CurrentlyActivity,
                            error.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            )
        } catch (e: Exception) {
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvCurrR) {
            layoutManager = LinearLayoutManager(this@CurrentlyActivity)
            adapter = mAdapter
        }
    }

    private fun mapResponseToLink(response: LinkResponse): Link =
        Link(
            inputlink = response.inputlink ?: "",
            inputgenre = response.inputgenre ?: "",
            inputstatus = response.inputstatus ?: "",
            inputtitle = response.inputtitle ?: ""
        )
}