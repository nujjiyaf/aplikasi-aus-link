package com.jila.auslink

import com.google.gson.annotations.SerializedName

data class LinkResponse(
    @field:SerializedName("id")
    val id: String? = null,
    @field:SerializedName("inputlink")
    val inputlink: String? = null,
    @field:SerializedName("inputtitle")
    val inputtitle: String? = null,
    @field:SerializedName("inputgenre")
    val inputgenre: String? = null,
    @field:SerializedName("inputstatus")
    val inputstatus: String? = null
)