package com.jila.auslink

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jila.auslink.UpdateLinkActivity.Companion.TAG_LINK
import com.jila.auslink.databinding.ItemLinkBinding

class LinkAdapter(private val nameClass: String) :
    RecyclerView.Adapter<LinkAdapter.LinkViewHolder>() {
    private val data = arrayListOf<Link>()

    fun setData(newData: List<Link>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class LinkViewHolder(private val view: ItemLinkBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Link) {
            view.subjudul1.text = data.inputtitle
            if (nameClass == SubGenreActivity::class.java.name) {
                view.statusBacaan.text = data.inputstatus
            } else {
                view.statusBacaan.text = data.inputgenre
            }
            view.btnEdit.setOnClickListener {
                val intent = Intent(itemView.context, UpdateLinkActivity::class.java)
                intent.putExtra(TAG_LINK, data)
                itemView.context.startActivity(intent)
            }
            view.btnContinue.setOnClickListener {
                try {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(data.inputlink)
                    itemView.context.startActivity(intent)
                } catch (e: Exception) {
                    Log.e("Adapter", e.message.toString())
                }
            }
        }
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder =
        LinkViewHolder(ItemLinkBinding.inflate(LayoutInflater.from(parent.context), parent, false))
}