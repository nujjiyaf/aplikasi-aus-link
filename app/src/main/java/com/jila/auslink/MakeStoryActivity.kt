package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.jila.auslink.databinding.ActivityMakeStoryBinding

class MakeStoryActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var binding: ActivityMakeStoryBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMakeStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAddStory.setOnClickListener {
            if (isValidForm()) {
                val add_judul = binding.addJudul.text.toString()
                val add_author = binding.addAuthor.text.toString()
                val add_story = binding.addStory.text.toString()

                database = FirebaseDatabase.getInstance().getReference("story")
                val story = Story(add_judul, add_author, add_story)
                FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                    database.child(userid).child(add_author).setValue(story)
                        .addOnSuccessListener {
                            binding.addJudul.text.clear()
                            binding.addAuthor.text.clear()
                            binding.addStory.text.clear()

                            Toast.makeText(this, "Succesfully Added", Toast.LENGTH_SHORT).show()
                            onBackPressed()
                        }.addOnFailureListener {
                            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                        }
                }
            } else {
                Toast.makeText(this, "Data not filled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isValidForm(): Boolean =
        binding.addJudul.text.isNotEmpty() && binding.addAuthor.text.isNotEmpty() && binding.addStory.text.isNotEmpty()
}