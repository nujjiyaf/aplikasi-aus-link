package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jila.auslink.MyStoryActivity.Companion.TAG_STORY
import com.jila.auslink.databinding.ActivityViewStoryBinding

class ViewStoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityViewStoryBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Story>(TAG_STORY)?.let {
            updateUI(it)
        }
    }

    private fun updateUI(data: Story) {
        binding.textViewJudul.text = data.addjudul
        binding.textViewAuthor.text = data.addauthor
        binding.textViewIsi.text = data.addstory
    }
}
