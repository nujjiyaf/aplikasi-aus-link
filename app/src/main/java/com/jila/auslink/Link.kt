package com.jila.auslink

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Link(
    val id: String = "",
    val inputlink: String,
    val inputtitle: String,
    val inputgenre: String,
    val inputstatus: String
) : Parcelable
