package com.jila.auslink

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jila.auslink.databinding.ActivityHomepageBinding

class HomepageActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityHomepageBinding
    private lateinit var bottom_navigation: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomepageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCurrentlyRead.setOnClickListener(this)
        binding.btnGenre.setOnClickListener(this)
        binding.btnStory.setOnClickListener(this)

        bottom_navigation = findViewById(R.id.bottomNavigationView)
        bottom_navigation.getMenu().findItem(R.id.homepageActivity).setChecked(true);
        bottom_navigation.setOnNavigationItemSelectedListener(menuItemSelected)

    }

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.homepageActivity -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.genreActivity -> {
                val intent = Intent(this, GenreActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.addLinkActivity -> {
                val intent = Intent(this, AddLinkActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.logoutActivity -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_currently_read -> run {
                val current = Intent(this@HomepageActivity, CurrentlyActivity::class.java)
                startActivity(current)
            }
            R.id.btn_genre -> run {
                val current = Intent(this@HomepageActivity, GenreActivity::class.java)
                startActivity(current)
            }
            R.id.btn_story -> run {
                val current = Intent(this@HomepageActivity, ListStoryActivity::class.java)
                startActivity(current)
            }
        }
    }

}