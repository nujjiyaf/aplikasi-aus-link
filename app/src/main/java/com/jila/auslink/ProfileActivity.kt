package com.jila.auslink

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.jila.auslink.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {
    private lateinit var bottom_navigation: BottomNavigationView
    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        FirebaseAuth.getInstance().currentUser?.displayName?.let { username ->
            binding.textDisplayName.text = "Hi, $username"
        }

        FirebaseAuth.getInstance().currentUser?.email?.let { email ->
            binding.textViewEmail.text = email
        }

        FirebaseAuth.getInstance().currentUser?.photoUrl?.let { photo ->
            Glide.with(this).load(photo).into(binding.imageView)
        }

        binding.buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build()
            var googleSignInClient = GoogleSignIn.getClient(this, gso)
            googleSignInClient.signOut()
            val intent = Intent(this, LoginActivity::class.java).also { intent ->
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            startActivity(intent)
        }

        binding.btnAddStory.setOnClickListener {
            val intent = Intent(this, MakeStoryActivity::class.java)
            startActivity(intent)
        }

        binding.btnMyStory.setOnClickListener {
            val intent = Intent(this, MyStoryActivity::class.java)
            startActivity(intent)
        }

        bottom_navigation = findViewById(R.id.bottomNavigationView)
        bottom_navigation.getMenu().findItem(R.id.logoutActivity).setChecked(true);
        bottom_navigation.setOnNavigationItemSelectedListener(menuItemSelected)
    }

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.logoutActivity -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.homepageActivity -> {
                val intent = Intent(this, HomepageActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.addLinkActivity -> {
                val intent = Intent(this, AddLinkActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.genreActivity -> {
                val intent = Intent(this, GenreActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}