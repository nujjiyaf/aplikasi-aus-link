package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jila.auslink.databinding.ActivitySubGenreBinding

class SubGenreActivity : AppCompatActivity() {
    private val mAdapter = LinkAdapter(this.javaClass.name)
    private lateinit var binding: ActivitySubGenreBinding
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubGenreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getStringExtra(GenreActivity.TAG_CATEGORY)?.let {
            updateUI(it)
        }

        try {
            intent.getStringExtra(GenreActivity.TAG_CATEGORY)?.let { category ->
                dbRef = FirebaseDatabase.getInstance().reference
                val userid = FirebaseAuth.getInstance().currentUser?.uid.toString()
                dbRef.child("link").child(userid).child(category).addValueEventListener(
                    object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val listLink = arrayListOf<Link>()

                            snapshot.children.forEach {
                                it.getValue(LinkResponse::class.java)?.let { response ->
                                    val transformedResponse = mapResponseToLink(response)
                                    listLink.add(transformedResponse)
                                }
                            }

                            mAdapter.setData(listLink)
                        }

                        override fun onCancelled(error: DatabaseError) {
                            Toast.makeText(this@SubGenreActivity, error.message, Toast.LENGTH_SHORT)
                                .show()
                        }

                    }
                )
            }
        } catch (e: Exception) {
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvItem) {
            layoutManager = LinearLayoutManager(this@SubGenreActivity)
            adapter = mAdapter
        }
    }

    private fun mapResponseToLink(response: LinkResponse): Link =
        Link(
            id = response.id ?: "",
            inputlink = response.inputlink ?: "",
            inputgenre = response.inputgenre ?: "",
            inputstatus = response.inputstatus ?: "",
            inputtitle = response.inputtitle ?: ""
        )

    private fun updateUI(data: String) {
        binding.textViewGenre.text = data
    }

}