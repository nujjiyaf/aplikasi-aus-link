package com.jila.auslink

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.jila.auslink.EditMyStoryActivity.Companion.TAG_STORY
import com.jila.auslink.databinding.ItemMystoryBinding

class MyStoryAdapter : RecyclerView.Adapter<MyStoryAdapter.MyStoryViewHolder>() {
    private lateinit var database: DatabaseReference
    private val data = arrayListOf<Story>()

    fun setData(newData: List<Story>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class MyStoryViewHolder(private val view: ItemMystoryBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Story) {
            view.txtJudulMystory.text = data.addjudul
            view.txtAuthorMystory.text = data.addauthor
            view.btnDeleteMystory.setOnClickListener {
                database = FirebaseDatabase.getInstance().getReference("story")
                FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                    database.child(userid).child(data.addauthor).removeValue()
                        .addOnSuccessListener {

//                            Toast.makeText(this, "Succesfully Succed", Toast.LENGTH_SHORT).show()
//                            onBackPressed()
                        }.addOnFailureListener {
//                            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            view.btnUpdateMystory.setOnClickListener {
                val intent = Intent(itemView.context, UpdateMyStoryActivity::class.java)
                intent.putExtra(TAG_STORY, data)
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: MyStoryViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyStoryViewHolder =
        MyStoryViewHolder(
            ItemMystoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}