package com.jila.auslink

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.jila.auslink.databinding.ActivityUpdateLinkBinding

class UpdateLinkActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var status_spinner: String
    private lateinit var binding: ActivityUpdateLinkBinding
    private lateinit var link: Link
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateLinkBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Link>(TAG_LINK)?.let {
            link = it
            updateUI(it)
        }

        binding.upgenreSpinner.isEnabled = false

        binding.upstatusSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    status_spinner = binding.upstatusSpinner.selectedItem.toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        binding.btnUpdate.setOnClickListener {
            val newLink = binding.updateLink.text.toString()
            val newStatus = binding.upstatusSpinner.selectedItem.toString()
            link = link.copy(inputlink = newLink, inputstatus = newStatus)
            database = FirebaseDatabase.getInstance().getReference("link")
            FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                database.child(userid).child(link.inputgenre).child(link.id).setValue(link)
                    .addOnSuccessListener {
                        binding.updateLink.text.clear()
                        binding.updateTitle.text.clear()

                        Toast.makeText(this, "Succesfully Added", Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }.addOnFailureListener {
                        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                    }
            }
        }

        binding.btnDelete.setOnClickListener {
            database = FirebaseDatabase.getInstance().getReference("link")
            FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                database.child(userid).child(link.inputgenre).child(link.id).removeValue()
                    .addOnSuccessListener {
                        binding.updateLink.text.clear()
                        binding.updateTitle.text.clear()
                        Toast.makeText(this, "Succesfully Delete", Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }.addOnFailureListener {
                        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    private fun updateUI(link: Link) {
        binding.updateTitle.setText(link.inputtitle)
        binding.updateLink.setText(link.inputlink)
        val indexGenre = resources.getStringArray(R.array.genre_array).indexOf(link.inputgenre)
        binding.upgenreSpinner.setSelection(indexGenre)
        val indexStatus = resources.getStringArray(R.array.status_array).indexOf(link.inputstatus)
        binding.upstatusSpinner.setSelection(indexStatus)
    }

    companion object {
        const val TAG_LINK = "TAG_LINK"
    }
}