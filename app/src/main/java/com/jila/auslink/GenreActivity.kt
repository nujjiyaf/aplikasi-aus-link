package com.jila.auslink

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jila.auslink.databinding.ActivityGenreBinding

class GenreActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var bottom_navigation: BottomNavigationView
    private lateinit var binding: ActivityGenreBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGenreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            btnRomance.setOnClickListener(this@GenreActivity)
            btnAngst.setOnClickListener(this@GenreActivity)
            btnComedy.setOnClickListener(this@GenreActivity)
            btnFantasy.setOnClickListener(this@GenreActivity)
            btnHorror.setOnClickListener(this@GenreActivity)
            btnMistery.setOnClickListener(this@GenreActivity)
            btnThriller.setOnClickListener(this@GenreActivity)
        }

        bottom_navigation = findViewById(R.id.bottomNavigationView)
        bottom_navigation.getMenu().findItem(R.id.genreActivity).setChecked(true);
        bottom_navigation.setOnNavigationItemSelectedListener(menuItemSelected)
    }

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.genreActivity -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.homepageActivity -> {
                val intent = Intent(this, HomepageActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.addLinkActivity -> {
                val intent = Intent(this, AddLinkActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.logoutActivity -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_romance -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Romance.name)
                startActivity(current)
            }
            R.id.btn_angst -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Angst.name)
                startActivity(current)
            }
            R.id.btn_comedy -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Comedy.name)
                startActivity(current)
            }
            R.id.btn_fantasy -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Fantasy.name)
                startActivity(current)
            }
            R.id.btn_mistery -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Mystery.name)
                startActivity(current)
            }
            R.id.btn_horror -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Horror.name)
                startActivity(current)
            }
            R.id.btn_thriller -> run {
                val current = Intent(this@GenreActivity, SubGenreActivity::class.java)
                current.putExtra(TAG_CATEGORY, Category.Thriller.name)
                startActivity(current)
            }
        }
    }

    enum class Category {
        Romance,
        Angst,
        Comedy,
        Fantasy,
        Mystery,
        Horror,
        Thriller
    }

    companion object {
        const val TAG_CATEGORY = "TAG_CATEGORY"
    }
}